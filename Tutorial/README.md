# COST FHIaims tutorial

Please use the following links to access the contents of this tutorial:

- gitlab repository: <https://gitlab.com/FHI-aims-club/tutorials/FHI-aims-for-transition-metal-oxides>

- documentation: <https://fhi-aims-club.gitlab.io/tutorials/FHI-aims-for-transition-metal-oxides>
